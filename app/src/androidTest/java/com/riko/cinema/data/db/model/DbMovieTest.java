package com.riko.cinema.data.db.model;

import org.greenrobot.greendao.test.AbstractDaoTestLongPk;

import com.riko.cinema.data.db.model.DbMovie;
import com.riko.cinema.data.db.model.DbMovieDao;

public class DbMovieTest extends AbstractDaoTestLongPk<DbMovieDao, DbMovie> {

    public DbMovieTest() {
        super(DbMovieDao.class);
    }

    @Override
    protected DbMovie createEntity(Long key) {
        DbMovie entity = new DbMovie();
        entity.setId(key);
        entity.setMovieId(1);
        return entity;
    }

}
