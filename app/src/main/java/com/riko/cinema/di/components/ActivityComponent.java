package com.riko.cinema.di.components;

import com.riko.cinema.di.PerActivity;
import com.riko.cinema.di.modules.ActivityModule;
import com.riko.cinema.ui.movie.MovieActivity;
import com.riko.cinema.ui.search.SearchActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = AppComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {
    void inject(SearchActivity searchActivity);
    void inject(MovieActivity movieActivity);
}
