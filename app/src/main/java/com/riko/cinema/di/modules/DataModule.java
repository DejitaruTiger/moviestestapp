package com.riko.cinema.di.modules;

import com.riko.cinema.data.DataManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;


@Module
public class DataModule {
    @Provides
    @Singleton
    DataManager provideDataManager() {
        return new DataManager();
    }
}
