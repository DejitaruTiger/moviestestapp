package com.riko.cinema.di.modules;


import com.riko.cinema.data.api.MoviesApi;
import com.riko.cinema.data.api.core.Api;
import com.riko.cinema.data.db.DbManager;
import com.riko.cinema.data.db.IDbManager;
import com.riko.cinema.data.prefs.IPreferenceManager;
import com.riko.cinema.data.prefs.PreferenceManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class DataManagerModule {

    @Provides
    @Singleton
    MoviesApi provideMovieApi() {
        return new Api().getMoviesApi();
    }

    @Provides
    @Singleton
    IDbManager provideDb() {
        return new DbManager();
    }

    @Provides
    @Singleton
    IPreferenceManager providePreferenceManager() {
        return new PreferenceManager();
    }
}
