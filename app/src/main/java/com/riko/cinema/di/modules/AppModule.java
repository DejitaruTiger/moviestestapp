package com.riko.cinema.di.modules;

import android.app.Application;
import android.content.Context;

import com.riko.cinema.util.Utils;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {
    private final Application mApplication;

    public AppModule(Application application) {
        mApplication = application;
    }

    @Provides
    @Singleton
    Application provideApplication() {
        return mApplication;
    }

    @Provides
    @Singleton
    Context provideContext() {
        return mApplication;
    }

    @Provides
    @Singleton
    Utils provideUtils(Context context) {
        return new Utils(context);
    }
}
