package com.riko.cinema.di.components;

import android.content.Context;

import com.riko.cinema.data.DataManager;
import com.riko.cinema.data.db.model.DbMovieDao;
import com.riko.cinema.di.modules.AppModule;
import com.riko.cinema.di.modules.DataModule;
import com.riko.cinema.di.modules.DbModule;
import com.riko.cinema.util.Utils;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, DataModule.class, DbModule.class})
public interface AppComponent {
    Context getContext();
    DataManager getDataManager();
    DbMovieDao getDbMovieDao();
    Utils getUtils();
}
