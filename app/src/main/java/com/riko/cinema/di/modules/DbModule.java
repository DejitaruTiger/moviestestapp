package com.riko.cinema.di.modules;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.riko.cinema.data.db.model.DaoMaster;
import com.riko.cinema.data.db.model.DaoSession;
import com.riko.cinema.data.db.model.DbMovieDao;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;


@Module
public class DbModule {
    @Provides
    @Singleton
    DaoSession provideDaoSession(Context context) {
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(context, "movie-db", null);
        SQLiteDatabase db = helper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        return daoMaster.newSession();
    }

    @Provides
    DbMovieDao provideDbMovieDao(DaoSession daoSession) {
        return daoSession.getDbMovieDao();
    }
}
