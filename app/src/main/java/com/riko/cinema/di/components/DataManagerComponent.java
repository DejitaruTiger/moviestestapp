package com.riko.cinema.di.components;

import com.riko.cinema.data.DataManager;
import com.riko.cinema.di.modules.DataManagerModule;

import javax.inject.Singleton;

import dagger.Component;


@Singleton
@Component(modules = {DataManagerModule.class})
public interface DataManagerComponent {
    void inject(DataManager dataManager);
}
