package com.riko.cinema.ui.search;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.riko.cinema.data.api.model.MovieList;
import com.riko.cinema.data.db.model.DbMovie;

import java.util.List;


public interface ISearchView extends MvpView {
    @StateStrategyType(OneExecutionStateStrategy.class)
    void showToast(String msg);
    @StateStrategyType(AddToEndSingleStrategy.class)
    void showSearchResults(MovieList movieList);
    @StateStrategyType(AddToEndSingleStrategy.class)
    void showFavoriteMovies(List<DbMovie> movieList);
    @StateStrategyType(OneExecutionStateStrategy.class)
    void showMovieDetails(int id);
    @StateStrategyType(OneExecutionStateStrategy.class)
    void showProgress();
    @StateStrategyType(OneExecutionStateStrategy.class)
    void hideProgress();
    @StateStrategyType(AddToEndSingleStrategy.class)
    void showDiscoverResults(MovieList body);
    @StateStrategyType(OneExecutionStateStrategy.class)
    void showRandomChoice(MovieList body);
}
