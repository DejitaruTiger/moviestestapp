package com.riko.cinema.ui.movie;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.riko.cinema.data.api.model.Genre;
import com.riko.cinema.data.api.model.MovieReviews;
import com.riko.cinema.data.api.model.Videos;

import java.util.List;


public interface IMovieView extends MvpView {
    @StateStrategyType(OneExecutionStateStrategy.class)
    void showToast(String msg);
    void setMovieTitle(String title);
    void setDescription(String description);
    void setPoster(String posterUrl);
    void setReleaseDate(String date);
    void setRate(String rate);
    @StateStrategyType(OneExecutionStateStrategy.class)
    void setFavoriteChecked(boolean checked);
    @StateStrategyType(OneExecutionStateStrategy.class)
    void showProgress();
    @StateStrategyType(OneExecutionStateStrategy.class)
    void hideProgress();
    void setGenres(List<Genre> genres);
    void setReviews(MovieReviews reviews);
    void setTrailer(String link);
}
