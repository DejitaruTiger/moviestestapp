package com.riko.cinema.ui.search;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.riko.cinema.App;
import com.riko.cinema.data.DataManager;

import java.util.Random;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;


@InjectViewState
public class SearchPresenter extends MvpPresenter<ISearchView> implements ISearchPresenter {
    @Inject DataManager dataManager;
    private CompositeDisposable disposable = new CompositeDisposable();

    public SearchPresenter() {
        dataManager = App.getAppComponent().getDataManager();
    }

    public void search(String query) {
        getViewState().showProgress();
        disposable.add(dataManager
                .searchMovies(query)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        response -> {
                            getViewState().hideProgress();
                            if(response.isSuccessful()) {
                                getViewState().showSearchResults(response.body());
                            } else {
                                getViewState().showToast("Error " + response.code());
                            }
                        }, e -> {
                            getViewState().showToast(e.getMessage());
                            getViewState().hideProgress();
                        }
                ));
    }

    public void discoverMovies(int page) {
        getViewState().hideProgress();
        disposable.add(dataManager
                .discoverMovies(page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    getViewState().hideProgress();
                    if(response.isSuccessful()) {
                        getViewState().showDiscoverResults(response.body());
                    } else {
                        getViewState().showToast("Error " + response.code());
                    }
                }, e -> {
                    getViewState().showToast(e.getMessage());
                    getViewState().hideProgress();
                }));
    }

    @Override
    public void showMovieDetails(int id) {
        getViewState().showMovieDetails(id);
    }

    @Override
    public void showFavoriteMovies() {
        getViewState().showFavoriteMovies(dataManager.getFavoriteMoviesList());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        disposable.dispose();
    }

    @Override
    public void randomChoice() {
        int page = new Random().nextInt(100-1) + 1;

        getViewState().hideProgress();
        disposable.add(dataManager
                .discoverMovies(page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    getViewState().hideProgress();
                    if(response.isSuccessful()) {
                        getViewState().showRandomChoice(response.body());
                    } else {
                        getViewState().showToast("Error " + response.code());
                    }
                }, e -> {
                    getViewState().showToast(e.getMessage());
                    getViewState().hideProgress();
                }));
    }
}
