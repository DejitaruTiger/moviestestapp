package com.riko.cinema.ui.movie;

import com.riko.cinema.data.api.model.Movie;


public interface IMoviePresenter {
    void getFilmDetails(int id);
    void fillViewFields(Movie movie);
    void favorite(boolean isAddToFavorites);
    void getFilmReviews(int id);
    void getFilmVideos(int id);
}
