package com.riko.cinema.ui.search;


public interface ISearchPresenter {
    void search(String query);
    void showMovieDetails(int id);
    void showFavoriteMovies();
    void randomChoice();
}
