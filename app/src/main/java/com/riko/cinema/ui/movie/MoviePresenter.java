package com.riko.cinema.ui.movie;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.riko.cinema.App;
import com.riko.cinema.R;
import com.riko.cinema.data.DataManager;
import com.riko.cinema.data.api.model.Movie;
import com.riko.cinema.data.api.model.MovieReviews;
import com.riko.cinema.data.api.model.Videos;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;


@InjectViewState
public class MoviePresenter extends MvpPresenter<IMovieView> implements IMoviePresenter {
    DataManager dataManager;
    private CompositeDisposable disposable = new CompositeDisposable();
    private Movie movie;

    public MoviePresenter() {
        dataManager = App.getAppComponent().getDataManager();
    }

    @Override
    public void getFilmDetails(int id) {
        getViewState().showProgress();
        disposable.add(dataManager
                .getMovieDetails(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        response -> {
                            getViewState().hideProgress();
                            if(!response.isSuccessful()) {
                                getViewState().showToast("Error " + response.code());
                                return;
                            }
                            fillViewFields(response.body());
                        }, e ->  {
                            getViewState().showToast(e.getMessage());
                            getViewState().hideProgress();
                        }
                ));
    }

    @Override
    public void getFilmReviews(int id) {
        getViewState().showProgress();
        disposable.add(dataManager
                .getMovieReviews(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        response -> {
                            getViewState().hideProgress();
                            if(!response.isSuccessful()) return;
                            fillReviews(response.body());
                        }, e ->  {
                            getViewState().showToast(e.getMessage());
                            getViewState().hideProgress();
                        }
                ));
    }

    @Override
    public void getFilmVideos(int id) {
        getViewState().showProgress();
        disposable.add(dataManager
                .getMovieVideos(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        response -> {
                            getViewState().hideProgress();
                            if(!response.isSuccessful()) return;
                            fillTrailer(response.body());
                        }, e ->  {
                            e.printStackTrace();
                            getViewState().hideProgress();
                        }
                ));
    }

    private void fillReviews(MovieReviews movieReviews) {
        getViewState().setReviews(movieReviews);
    }

    private void fillTrailer(Videos videos) {
        for (Videos.Trailer trailer : videos.getResults()) {
            String link = trailer.generateYouTubeLink();
            if (link != null) {
                getViewState().setTrailer(link);
                break;
            }
        }
    }

    @Override
    public void fillViewFields(Movie movie) {
        this.movie = movie;
        getViewState().setMovieTitle(movie.getTitle());
        getViewState().setDescription(movie.getOverview());
        getViewState().setPoster(movie.getPosterPath());
        getViewState().setReleaseDate(movie.getReleaseDate());
        getViewState().setRate(movie.getRate());
        getViewState().setGenres(movie.getGenres());

        checkFavoriteStatus();
    }

    @Override
    public void favorite(boolean isAddToFavorites) {
        if(isAddToFavorites) {
            addToFavorite();
        } else {
            removeFromFavorite();
        }
    }

    private void addToFavorite() {
        dataManager.addToFavorites(movie);
    }

    private void removeFromFavorite() {
        dataManager.removeFromFavorites(movie);
    }

    private void checkFavoriteStatus() {
        getViewState().setFavoriteChecked(dataManager.checkIsFavorite(movie));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        disposable.dispose();
    }
}
