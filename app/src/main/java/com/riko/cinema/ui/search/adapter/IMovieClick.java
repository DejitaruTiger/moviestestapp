package com.riko.cinema.ui.search.adapter;


public interface IMovieClick {
    void movieClicked(int id);
}
