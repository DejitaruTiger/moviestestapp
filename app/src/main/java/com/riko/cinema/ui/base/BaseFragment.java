package com.riko.cinema.ui.base;



import android.app.ProgressDialog;
import android.content.Context;
import android.support.annotation.IdRes;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatFragment;


public class BaseFragment extends MvpAppCompatFragment {
    private ProgressDialog dialog;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        dialog = new ProgressDialog(getActivity());
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
    }

    @SuppressWarnings({"unchecked", "ConstantConditions"})
    public <V extends View> V bind(@IdRes int id) {
        return (V) getView().findViewById(id);
    }

    public void hideKeyboard() {
        if(getActivity() == null) return;
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void showToast(String msg) {
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
    }

    @SuppressWarnings("ConstantConditions")
    public void showSnackbar(String text) {
        Snackbar.make(getView(), text, Snackbar.LENGTH_SHORT).show();
    }

    public void showProgress() {
        try {
            dialog.show();
        } catch (IllegalArgumentException ignore) {}
    }

    public void hideProgress() {
        try {
            dialog.dismiss();
        } catch (IllegalArgumentException ignore) {}
    }
}
