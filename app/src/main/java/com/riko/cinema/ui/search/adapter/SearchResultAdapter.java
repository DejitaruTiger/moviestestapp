package com.riko.cinema.ui.search.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.riko.cinema.R;
import com.riko.cinema.data.api.model.Movie;

import java.util.List;


public class SearchResultAdapter extends RecyclerView.Adapter<SearchResultAdapter.ViewHolder> {
    private List<Movie> movieList;
    private IMovieClick movieClick;

    public SearchResultAdapter(List<Movie> movieList, IMovieClick movieClick) {
        this.movieList = movieList;
        this.movieClick = movieClick;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_search_result, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.fillWith(movieList.get(position));
    }

    @Override
    public int getItemCount() {
        if(movieList == null) return 0;
        return movieList.size();
    }

    public void setMovies(List<Movie> movies) {
        movieList.addAll(movies);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private int movieId;
        private TextView mNameTv, mRateTv;
        private ImageView mLogoIv;


        public ViewHolder(final View itemView) {
            super(itemView);
            mLogoIv = (ImageView) itemView.findViewById(R.id.search_adapter_logo_iv);
            mRateTv = (TextView) itemView.findViewById(R.id.search_adapter_rate_tv);
            mNameTv = (TextView) itemView.findViewById(R.id.search_adapter_name_tv);

            itemView.setOnClickListener(view -> movieClick.movieClicked(movieId));
        }

        public void fillWith(Movie movie) {
            movieId = movie.getId();
            mNameTv.setText(movie.getTitle());
            mRateTv.setText(String.valueOf(movie.getRate()) + "★");
            Glide.with(mLogoIv.getContext())
                    .load(movie.getPosterPath())
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            e.printStackTrace();
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            return false;
                        }
                    })
                    .into(mLogoIv);
        }
    }
}
