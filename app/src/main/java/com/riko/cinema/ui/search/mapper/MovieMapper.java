package com.riko.cinema.ui.search.mapper;

import com.riko.cinema.data.api.model.Movie;
import com.riko.cinema.data.db.model.DbMovie;

import java.util.ArrayList;
import java.util.List;

public class MovieMapper {
    public static ArrayList<Movie> mapDbMoviesToMovies(List<DbMovie> dbMovies) {
        ArrayList<Movie> movies = new ArrayList<>();

        for (DbMovie dbMovie : dbMovies) {
            Movie movie = new Movie();
            movie.setId(dbMovie.getMovieId());
            movie.setTitle(dbMovie.getTitle());
            movie.setOverview(dbMovie.getOverview());
            movie.setPosterPath(dbMovie.getPosterPath());
            movie.setRate(Double.valueOf(dbMovie.getRate()));
            movie.setReleaseDate(dbMovie.getReleaseDate());

            movies.add(movie);
        }

        return movies;
    }
}
