package com.riko.cinema.ui.search;

import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.paginate.Paginate;
import com.riko.cinema.R;
import com.riko.cinema.data.api.model.Movie;
import com.riko.cinema.data.api.model.MovieList;
import com.riko.cinema.data.db.model.DbMovie;
import com.riko.cinema.ui.base.BaseActivity;
import com.riko.cinema.ui.movie.MovieActivity;
import com.riko.cinema.ui.search.adapter.IMovieClick;
import com.riko.cinema.ui.search.adapter.SearchResultAdapter;
import com.riko.cinema.ui.search.mapper.MovieMapper;

import java.util.List;
import java.util.Random;

public class SearchActivity extends BaseActivity implements ISearchView, IMovieClick {
    @InjectPresenter SearchPresenter presenter;
    private RecyclerView mMovieRecyclerView;
    private SearchResultAdapter mAdapter;
    private boolean mIsLoading = false;
    private int mPage = 1;
    private Paginate mPaginate;
    private boolean isFavoriteShown = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        setTitle("Movies");

        init();
        SharedPreferences prefs = getSharedPreferences("Movies", MODE_PRIVATE);
        if (prefs.getBoolean("dialog", true)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(R.string.first_launch);
            builder.setPositiveButton(android.R.string.ok, null);
            builder.create().show();
            prefs.edit().putBoolean("dialog", false).apply();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isFavoriteShown) {
            isFavoriteShown = false;
            presenter.showFavoriteMovies();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = null;
        SearchView.OnQueryTextListener queryTextListener;

        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

            queryTextListener = new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextChange(String newText) {
                    return true;
                }

                @Override
                public boolean onQueryTextSubmit(String query) {
                    presenter.search(query);
                    return true;
                }
            };
            searchView.setOnQueryTextListener(queryTextListener);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_star) {
            presenter.showFavoriteMovies();
        } else if (item.getItemId() == R.id.action_choose) {
            presenter.randomChoice();
        }
        return super.onOptionsItemSelected(item);
    }

    protected void init() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mMovieRecyclerView = bind(R.id.search_recyclerview);
        mMovieRecyclerView.setLayoutManager(layoutManager);

        discover();

    }

    private void discover() {
        isFavoriteShown = false;
        mIsLoading = true;
        presenter.discoverMovies(mPage);
    }

    @Override
    public void showRandomChoice(MovieList body) {
        Movie item = body.getMovieList().get(new Random().nextInt(body.getMovieList().size()));
        showMovieDetails(item.getId());
    }

    private void initPaging() {
        Paginate.Callbacks callbacks = new Paginate.Callbacks() {
            @Override
            public void onLoadMore() {
                // Load next mPage of data (e.g. network or database)
                discover();
            }

            @Override
            public boolean isLoading() {
                // Indicate whether new mPage mIsLoading is in progress or not
                return mIsLoading;
            }

            @Override
            public boolean hasLoadedAllItems() {
                // Indicate whether all data (pages) are loaded or not
                return false;
            }
        };

        mPaginate = Paginate.with(mMovieRecyclerView, callbacks)
                .setLoadingTriggerThreshold(3)
                .addLoadingListItem(true)
                .build();
    }

    @Override
    public void movieClicked(int id) {
        presenter.showMovieDetails(id);
    }

    @Override
    public void showSearchResults(MovieList movieList) {
        mIsLoading = false;
        mAdapter = new SearchResultAdapter(movieList.getMovieList(), this);
        mMovieRecyclerView.setAdapter(mAdapter);
        if (mPaginate != null) {
            mPaginate.unbind();
        }
    }

    @Override
    public void showDiscoverResults(MovieList movieList) {
        mIsLoading = false;

        if (mPage == 1) {
            mAdapter = new SearchResultAdapter(movieList.getMovieList(), this);
            mMovieRecyclerView.setAdapter(mAdapter);
            initPaging();
        } else {
            if (isFavoriteShown) {
                mPage = 1;
                return;
            }
            mAdapter.setMovies(movieList.getMovieList());
        }
        mPage += 1;
    }

    @Override
    public void showFavoriteMovies(List<DbMovie> movieList) {
        if (isFavoriteShown) {
            mPage = 1;
            discover();
            return;
        }

        mAdapter = new SearchResultAdapter(MovieMapper.mapDbMoviesToMovies(movieList), this);
        mMovieRecyclerView.setAdapter(mAdapter);
        isFavoriteShown = true;
        if (mPaginate != null) {
            mPaginate.unbind();
        }
    }

    @Override
    public void showMovieDetails(int id) {
        MovieActivity.start(this, id);
    }

    @Override
    public void onBackPressed() {
        if (isFavoriteShown) {
            mPage = 1;
            discover();
        } else {
            super.onBackPressed();
        }
    }
}
