package com.riko.cinema.ui.movie;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.bumptech.glide.Glide;
import com.riko.cinema.R;
import com.riko.cinema.data.api.model.Genre;
import com.riko.cinema.data.api.model.MovieReviews;
import com.riko.cinema.ui.base.BaseActivity;

import java.util.List;

public class MovieActivity extends BaseActivity implements IMovieView {
    public static final String EXTRA_ID = "id";

    @InjectPresenter MoviePresenter presenter;
    private TextView mDescriptionTv, mDateTv, mRateTv, mGenresTv, mReviews, mYouTube;
    private CheckBox mRateCheckbox;
    private ImageView mPosterIv;

    /**
    *    @param id - id of movie
    **/
    public static void start(Context context, int id) {
        Intent starter = new Intent(context, MovieActivity.class);
        starter.putExtra(EXTRA_ID, id);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        init();
        presenter.getFilmDetails(getMovieId());
        presenter.getFilmReviews(getMovieId());
        presenter.getFilmVideos(getMovieId());
    }

    protected void init() {
        mDescriptionTv = bind(R.id.movie_description_tv);
        mDateTv = bind(R.id.movie_date_tv);
        mPosterIv = bind(R.id.movie_poster_iv);
        mRateTv = bind(R.id.movie_rate_tv);
        mRateCheckbox = bind(R.id.movie_rate_checkbox);
        mGenresTv = bind(R.id.genres);
        mReviews = bind(R.id.comments);
        mYouTube = bind(R.id.youtube);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void setMovieTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    @Override
    public void setGenres(List<Genre> genres) {
        StringBuilder genresTitle = new StringBuilder();
        for (Genre g : genres) {
            genresTitle.append(g.getTitle()).append("\n");
        }
        mGenresTv.setText(getString(R.string.genres) + genresTitle.toString());
    }

    @Override
    public void setReviews(MovieReviews reviews) {
        if (reviews.getReviewArrayList() == null || reviews.getReviewArrayList().isEmpty()) return;
        StringBuilder comments = new StringBuilder();
        for (MovieReviews.Review r : reviews.getReviewArrayList()) {
            comments.append(r.getAuthor()).append("\n").append(r.getContent()).append("\n\n");
        }
        mReviews.setText(getString(R.string.comments) + comments.toString());
    }

    @Override
    public void setTrailer(String link) {
        mYouTube.setVisibility(View.VISIBLE);
        mYouTube.setOnClickListener(v -> {
            Intent webIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
            startActivity(webIntent);
        });
    }

    @Override
    public void setDescription(String description) {
        mDescriptionTv.setText(description);
    }

    @Override
    public void setPoster(String posterUrl) {
        Glide.with(this)
                .load(posterUrl)
                .into(mPosterIv);
    }

    @Override
    public void setReleaseDate(String date) {
        mDateTv.setText(getString(R.string.date) + date);
    }

    @Override
    public void setRate(String rate) {
        mRateTv.setText(rate + "★");
    }

    @Override
    public void setFavoriteChecked(boolean checked) {
        mRateCheckbox.setChecked(checked);
        mRateCheckbox.setOnCheckedChangeListener((view, isChecked) -> presenter.favorite(isChecked));
    }

    public int getMovieId() {
        return getIntent().getIntExtra(EXTRA_ID, -1);
    }
}
