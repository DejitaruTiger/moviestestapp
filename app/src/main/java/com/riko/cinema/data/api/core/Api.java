package com.riko.cinema.data.api.core;

import com.riko.cinema.BuildConfig;
import com.riko.cinema.data.api.MoviesApi;

import java.io.IOException;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


public class Api {
    private final String BASE_URL = "https://api.themoviedb.org/";
    public static final String POSTER_URL = "https://image.tmdb.org/t/p/w500/";
    private Requests requests;
    private MoviesApi moviesApi;

    public Api() {
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .readTimeout(5, TimeUnit.MINUTES)
                .writeTimeout(5, TimeUnit.MINUTES)
                .addInterceptor(new ApiKeyInterceptor())
                .addInterceptor(new LangInterceptor())
                .addInterceptor(getHttpLoggingInterceptor())
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .baseUrl(BASE_URL)
                .build();

        requests = retrofit.create(Requests.class);
    }

    public MoviesApi getMoviesApi() {
        if(moviesApi == null) {
            moviesApi = new MoviesApi(requests);
        }
        return moviesApi;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////// INTERCEPTORS /////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////

    private HttpLoggingInterceptor getHttpLoggingInterceptor() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        if (BuildConfig.DEBUG) {
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        } else {
            logging.setLevel(HttpLoggingInterceptor.Level.NONE);
        }
        return logging;
    }

    private class ApiKeyInterceptor implements Interceptor {
        @Override
        public Response intercept(Chain chain) throws IOException {
            Request original = chain.request();
            HttpUrl originalUrl = original.url();

            HttpUrl url = originalUrl.newBuilder()
                    .addQueryParameter("api_key", BuildConfig.apiKey)
                    .build();
            Request.Builder builder = original.newBuilder()
                    .url(url);
            return chain.proceed(builder.build());
        }
    }

    private class LangInterceptor implements Interceptor {
        @Override
        public Response intercept(Chain chain) throws IOException {
            Request original = chain.request();
            HttpUrl originalUrl = original.url();

            HttpUrl url = originalUrl.newBuilder()
//                    .addQueryParameter("language", Locale.getDefault().getLanguage())
                    .addQueryParameter("language", "uk")
                    .build();
            Request.Builder builder = original.newBuilder()
                    .url(url);
            return chain.proceed(builder.build());
        }
    }
}
