package com.riko.cinema.data.api;

import com.riko.cinema.data.api.core.Requests;
import com.riko.cinema.data.api.model.Movie;
import com.riko.cinema.data.api.model.MovieList;
import com.riko.cinema.data.api.model.MovieReviews;
import com.riko.cinema.data.api.model.Videos;

import io.reactivex.Observable;
import retrofit2.Response;


public class MoviesApi implements IMoviesApi {
    public static final String URL_SEARCH_MOVIES = "https://api.themoviedb.org/3/search/movie";
    public static final String URL_MOVIE_DETAILS = "https://api.themoviedb.org/3/movie/{id}";
    public static final String URL_MOVIE_DISCOVER = "https://api.themoviedb.org/3/discover/movie";
    public static final String URL_MOVIE_REVIEWS = "https://api.themoviedb.org/3/movie/{id}/reviews";
    public static final String URL_MOVIE_VIDEOS = "https://api.themoviedb.org/3/movie/{id}/videos";

    private Requests requests;

    public MoviesApi(Requests requests) {
        this.requests = requests;
    }

    @Override
    public Observable<Response<MovieList>> searchMovies(String query) {
        return requests.searchMovies(query);
    }

    @Override
    public Observable<Response<MovieList>> discoverMovies(int page) {
        return requests.discoverMovies(page);
    }

    @Override
    public Observable<Response<Movie>> getMovieDetails(int id) {
        return requests.getMovieDetails(id);
    }

    @Override
    public Observable<Response<MovieReviews>> getMovieReviews(int id) {
        return requests.getMovieReviews(id);
    }

    @Override
    public Observable<Response<Videos>> getMovieVideos(int id) {
        return requests.getMovieVideos(id);
    }
}
