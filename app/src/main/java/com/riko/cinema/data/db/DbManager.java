package com.riko.cinema.data.db;


import com.riko.cinema.App;
import com.riko.cinema.data.api.model.Movie;
import com.riko.cinema.data.db.model.DbMovie;
import com.riko.cinema.data.db.model.DbMovieDao;

import java.util.List;

public class DbManager implements IDbManager {
    private DbMovieDao dbMovieDao;

    public DbManager() {
        dbMovieDao = App.getAppComponent().getDbMovieDao();
    }

    @Override
    public void addToFavorites(Movie movie) {
        DbMovie dbMovie = new DbMovie();

        dbMovie.setMovieId(movie.getId());
        dbMovie.setTitle(movie.getTitle());
        dbMovie.setPosterPath(movie.getRawPosterPath());
        dbMovie.setOverview(movie.getOverview());
        dbMovie.setRate(movie.getRate());
        dbMovie.setReleaseDate(movie.getReleaseDate());

        dbMovieDao.insert(dbMovie);
    }

    @Override
    public void removeFromFavorites(Movie movie) {
        List<DbMovie> dbMovieList = dbMovieDao
                .queryBuilder()
                .where(DbMovieDao.Properties.MovieId.eq(movie.getId()))
                .list();
        dbMovieDao.deleteInTx(dbMovieList);
    }

    @Override
    public boolean checkIsFavorite(Movie movie) {
        return dbMovieDao
                .queryBuilder()
                .where(DbMovieDao.Properties.MovieId.eq(movie.getId()))
                .count() > 0;
    }

    @Override
    public List<DbMovie> getFavoriteMoviesList() {
        return dbMovieDao.queryBuilder().list();
    }
}
