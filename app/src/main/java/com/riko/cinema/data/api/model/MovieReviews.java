package com.riko.cinema.data.api.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class MovieReviews {
    @SerializedName("results") ArrayList<Review> mReviewArrayList;

    public ArrayList<Review> getReviewArrayList() {
        return mReviewArrayList;
    }

    public class Review {
        @SerializedName("author") String author;
        @SerializedName("content") String content;

        public String getAuthor() {
            return author;
        }

        public String getContent() {
            return content;
        }
    }
}
