package com.riko.cinema.data.api.model;

import com.google.gson.annotations.SerializedName;

public class Genre {
    @SerializedName("id") int id;
    @SerializedName("name") String title;

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }
}
