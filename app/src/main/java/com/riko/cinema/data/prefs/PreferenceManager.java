package com.riko.cinema.data.prefs;

import android.content.Context;
import android.content.SharedPreferences;

import com.riko.cinema.App;

import javax.inject.Inject;


public class PreferenceManager implements IPreferenceManager {
    @Inject Context context;

    public PreferenceManager() {
        context = App.getAppComponent().getContext();
    }

    private String prefReadString(String name, String param, String deflt) {
        SharedPreferences shared = context.getSharedPreferences(name, Context.MODE_PRIVATE);
        return shared.getString(param, deflt);
    }

    private void prefWriteString(String name, String param, String value) {
        SharedPreferences prefs = context.getSharedPreferences(name, Context.MODE_PRIVATE);
        SharedPreferences.Editor ed = prefs.edit();
        ed.putString(param, value);
        ed.apply();
    }

    private boolean prefReadBool(String name, String param, boolean dflt) {
        SharedPreferences shared = context.getSharedPreferences(name, Context.MODE_PRIVATE);
        return shared.getBoolean(param, dflt);
    }

    private void prefWriteBool(String name, String param, boolean value) {
        SharedPreferences prefs = context.getSharedPreferences(name, Context.MODE_PRIVATE);
        SharedPreferences.Editor ed = prefs.edit();
        ed.putBoolean(param, value);
        ed.apply();
    }

    private void prefWriteInt(String name, String param, int value) {
        SharedPreferences prefs = context.getSharedPreferences(name, Context.MODE_PRIVATE);
        SharedPreferences.Editor ed = prefs.edit();
        ed.putInt(param, value).apply();
    }

    private int prefReadInt(String name, String param, int deflt) {
        SharedPreferences prefs = context.getSharedPreferences(name, Context.MODE_PRIVATE);
        return prefs.getInt(param, deflt);
    }
}
