package com.riko.cinema.data.api.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.riko.cinema.data.api.core.Api;

import java.util.List;


public class Movie implements Parcelable {
    @SerializedName("poster_path")
    String posterPath;

    @SerializedName("overview")
    String overview;

    @SerializedName("release_date")
    String releaseDate;

    @SerializedName("title")
    String title;

    @SerializedName("vote_average")
    double rate;

    @SerializedName("id")
    int id;

    @SerializedName("genres")
    List<Genre> genres;

    public Movie() {}

    public int getId() {
        return id;
    }

    public String getPosterPath() {
        return (posterPath != null ? Api.POSTER_URL + posterPath : "");
    }

    public String getRawPosterPath() {
        return posterPath;
    }

    public String getOverview() {
        return (overview != null ? overview : "");
    }

    public String getReleaseDate() {
        return (releaseDate != null ? releaseDate : "");
    }

    public String getTitle() {
        return (title != null ? title : "");
    }

    public String getRate() {
        return (rate != 0 ? String.valueOf(rate) : "");
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Genre> getGenres() {
        return genres;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.posterPath);
        dest.writeString(this.overview);
        dest.writeString(this.releaseDate);
        dest.writeString(this.title);
        dest.writeDouble(this.rate);
        dest.writeInt(this.id);
    }

    protected Movie(Parcel in) {
        this.posterPath = in.readString();
        this.overview = in.readString();
        this.releaseDate = in.readString();
        this.title = in.readString();
        this.rate = in.readDouble();
        this.id = in.readInt();
    }

    public static final Parcelable.Creator<Movie> CREATOR = new Parcelable.Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel source) {
            return new Movie(source);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };
}
