package com.riko.cinema.data.api.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Videos {
    @SerializedName("results") private ArrayList<Trailer> results;

    public ArrayList<Trailer> getResults() {
        return results;
    }

    public class Trailer {
        @SerializedName("key") private String key;
        @SerializedName("site") private String site;

        public String generateYouTubeLink() {
            if (site == null || !site.equals("YouTube") || key == null || key.isEmpty()) return null;
            return "https://www.youtube.com/watch?v=" + key;
        }

        public String getKey() {
            return key;
        }

        public String getSite() {
            return site;
        }
    }
}
