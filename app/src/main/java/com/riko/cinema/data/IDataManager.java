package com.riko.cinema.data;


import com.riko.cinema.data.api.IMoviesApi;
import com.riko.cinema.data.db.IDbManager;

public interface IDataManager extends IDbManager, IMoviesApi {
}
