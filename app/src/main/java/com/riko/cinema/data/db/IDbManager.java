package com.riko.cinema.data.db;


import com.riko.cinema.data.api.model.Movie;
import com.riko.cinema.data.db.model.DbMovie;

import java.util.List;

public interface IDbManager {
    void addToFavorites(Movie movie);
    void removeFromFavorites(Movie movie);
    boolean checkIsFavorite(Movie movie);
    List<DbMovie> getFavoriteMoviesList();
}
