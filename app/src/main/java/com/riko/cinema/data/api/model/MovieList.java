package com.riko.cinema.data.api.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class MovieList {
    @SerializedName("results")
    List<Movie> movieList;

    @SerializedName("page")
    int page;

    public List<Movie> getMovieList() {
        return movieList;
    }

    public int getPage() {
        return page;
    }
}
