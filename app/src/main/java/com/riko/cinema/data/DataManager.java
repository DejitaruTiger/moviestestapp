package com.riko.cinema.data;

import com.riko.cinema.App;
import com.riko.cinema.data.api.MoviesApi;
import com.riko.cinema.data.api.model.Movie;
import com.riko.cinema.data.api.model.MovieList;
import com.riko.cinema.data.api.model.MovieReviews;
import com.riko.cinema.data.api.model.Videos;
import com.riko.cinema.data.db.IDbManager;
import com.riko.cinema.data.db.model.DbMovie;
import com.riko.cinema.data.prefs.IPreferenceManager;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import retrofit2.Response;


public class DataManager implements IDataManager {
    @Inject MoviesApi apiManager;
    @Inject IDbManager dbManager;
    @Inject IPreferenceManager preferenceManager;

    public DataManager() {
        App.getDataManagerComponent().inject(this);
    }

    ////////////////////////////////////// DB //////////////////////////////////////////////////////
    @Override
    public void addToFavorites(Movie movie) {
        dbManager.addToFavorites(movie);
    }

    @Override
    public void removeFromFavorites(Movie movie) {
        dbManager.removeFromFavorites(movie);
    }

    @Override
    public boolean checkIsFavorite(Movie movie) {
        return dbManager.checkIsFavorite(movie);
    }

    @Override
    public List<DbMovie> getFavoriteMoviesList() {
        return dbManager.getFavoriteMoviesList();
    }

    ///////////////////////////////////// API //////////////////////////////////////////////////////
    @Override
    public Observable<Response<Movie>> getMovieDetails(int id) {
        return apiManager.getMovieDetails(id);
    }

    @Override
    public Observable<Response<MovieList>> searchMovies(String query) {
        return apiManager.searchMovies(query);
    }

    @Override
    public Observable<Response<MovieList>> discoverMovies(int page) {
        return apiManager.discoverMovies(page);
    }

    @Override
    public Observable<Response<MovieReviews>> getMovieReviews(int id) {
        return apiManager.getMovieReviews(id);
    }

    @Override
    public Observable<Response<Videos>> getMovieVideos(int id) {
        return apiManager.getMovieVideos(id);
    }
}
