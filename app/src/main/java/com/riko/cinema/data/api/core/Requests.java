package com.riko.cinema.data.api.core;

import com.riko.cinema.data.api.MoviesApi;
import com.riko.cinema.data.api.model.Movie;
import com.riko.cinema.data.api.model.MovieList;
import com.riko.cinema.data.api.model.MovieReviews;
import com.riko.cinema.data.api.model.Videos;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface Requests {

    @GET(MoviesApi.URL_SEARCH_MOVIES)
    Observable<Response<MovieList>> searchMovies(@Query("query") String searchQuery);

    @GET(MoviesApi.URL_MOVIE_DETAILS)
    Observable<Response<Movie>> getMovieDetails(@Path("id") int id);

    @GET(MoviesApi.URL_MOVIE_REVIEWS)
    Observable<Response<MovieReviews>> getMovieReviews(@Path("id") int id);

    @GET(MoviesApi.URL_MOVIE_VIDEOS)
    Observable<Response<Videos>> getMovieVideos(@Path("id") int id);

    @GET(MoviesApi.URL_MOVIE_DISCOVER)
    Observable<Response<MovieList>> discoverMovies(@Query("page") int page);

//    @FormUrlEncoded
//    @POST(UserApi.URL_AUTH)
//    Observable<Response<AuthModel>> auth(@Field("grant_type") String grantType,
//                                         @Field("client_id") String clientId,
//                                         @Field("client_secret") String clientSecret);
////
}
