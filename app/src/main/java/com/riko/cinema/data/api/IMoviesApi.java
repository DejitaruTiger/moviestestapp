package com.riko.cinema.data.api;

import com.riko.cinema.data.api.model.Movie;
import com.riko.cinema.data.api.model.MovieList;
import com.riko.cinema.data.api.model.MovieReviews;
import com.riko.cinema.data.api.model.Videos;

import io.reactivex.Observable;
import retrofit2.Response;

public interface IMoviesApi {
    Observable<Response<Movie>> getMovieDetails(int id);
    Observable<Response<MovieList>> searchMovies(String query);
    Observable<Response<MovieList>> discoverMovies(int page);
    Observable<Response<MovieReviews>> getMovieReviews(int id);
    Observable<Response<Videos>> getMovieVideos(int id);
}
