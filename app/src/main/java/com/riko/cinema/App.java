package com.riko.cinema;

import android.app.Application;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

import com.riko.cinema.di.components.ActivityComponent;
import com.riko.cinema.di.components.AppComponent;
import com.riko.cinema.di.components.DaggerActivityComponent;
import com.riko.cinema.di.components.DaggerAppComponent;
import com.riko.cinema.di.components.DaggerDataManagerComponent;
import com.riko.cinema.di.components.DataManagerComponent;
import com.riko.cinema.di.modules.AppModule;

public class App extends Application {
    private static AppComponent appComponent;
    private static ActivityComponent activityComponent;
    private static DataManagerComponent dataManagerComponent;

    public App() {
        appComponent = DaggerAppComponent
                .builder()
                .appModule(new AppModule(this))
                .build();
        activityComponent = DaggerActivityComponent
                .builder()
                .appComponent(appComponent)
                .build();
        dataManagerComponent = DaggerDataManagerComponent
                .builder()
                .build();
    }

    public static AppComponent getAppComponent() {
        return appComponent;
    }

    public static ActivityComponent getActivityComponent() {
        return activityComponent;
    }

    public static DataManagerComponent getDataManagerComponent() {
        return dataManagerComponent;
    }

    @VisibleForTesting
    public static void setAppComponent(@NonNull AppComponent component) {
        appComponent = component;
    }
}
