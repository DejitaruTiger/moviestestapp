package com.riko.cinema;

import com.riko.cinema.dagger.TestAppComponent;
import com.riko.cinema.dagger.TestComponentRule;
import com.riko.cinema.data.api.model.Movie;
import com.riko.cinema.data.api.model.MovieList;
import com.riko.cinema.ui.movie.IMovieView$$State;
import com.riko.cinema.ui.movie.MoviePresenter;
import com.riko.cinema.ui.search.ISearchView$$State;
import com.riko.cinema.ui.search.SearchPresenter;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.annotations.NonNull;
import io.reactivex.annotations.Nullable;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.schedulers.ExecutorScheduler;
import io.reactivex.plugins.RxJavaPlugins;
import okhttp3.MediaType;
import okhttp3.ResponseBody;
import okio.BufferedSource;
import retrofit2.Response;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


public class MoviePresenterTest extends BaseTest {
    TestAppComponent testAppComponent = new TestAppComponent();
    @Rule public TestComponentRule testComponentRule = new TestComponentRule(testAppComponent);

    @Mock IMovieView$$State movieView$$State;
    MoviePresenter moviePresenter;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);

        moviePresenter = new MoviePresenter();
        moviePresenter.setViewState(movieView$$State);
    }

    @Test
    public void fillMovieTest() {
        Movie movie = getTestMovie();
        Response response = Response.success(movie);

        when(testAppComponent.getDataManager().getMovieDetails(1)).thenReturn(Observable.just(response));
        moviePresenter.getFilmDetails(1);

        verify(movieView$$State).showProgress();
        verify(movieView$$State).hideProgress();
        verify(movieView$$State).setReleaseDate(movie.getReleaseDate());
        verify(movieView$$State).setDescription(movie.getOverview());
        verify(movieView$$State).setMovieTitle(movie.getTitle());
        verify(movieView$$State).setPoster(movie.getPosterPath());
        verify(movieView$$State).setRate(movie.getRate());
    }

    @Test
    public void fillMovieTest_Error() {
        Observable<Response<Movie>> obs = Observable.just(Response.error(404, new ResponseBody() {
            @Nullable
            @Override
            public MediaType contentType() {
                return null;
            }

            @Override
            public long contentLength() {
                return 0;
            }

            @Override
            public BufferedSource source() {
                return null;
            }
        }));

        when(testAppComponent.getDataManager().getMovieDetails(1)).thenReturn(obs);
        moviePresenter.getFilmDetails(1);

        verify(movieView$$State).showProgress();
        verify(movieView$$State).hideProgress();
        verify(movieView$$State).showToast("Error " + 404);
    }

    @Test
    public void favoriteCheckingTest() {
        Movie movie = getTestMovie();

        Response response = Response.success(movie);
        when(testAppComponent.getDataManager().getMovieDetails(1)).thenReturn(Observable.just(response));
        when(testAppComponent.getDataManager().checkIsFavorite(movie)).thenReturn(true);

        moviePresenter.getFilmDetails(1);

        verify(movieView$$State).setFavoriteChecked(true);
    }

    @Test(expected = ExecutionException.class)
    public void addFavoriteTest() {
        Movie movie = getTestMovie();
        Response response = Response.success(movie);

        when(testAppComponent.getDataManager().getMovieDetails(1)).thenReturn(Observable.just(response));
        doThrow(ExecutionException.class).when(testAppComponent.getDataManager()).addToFavorites(movie);

        moviePresenter.getFilmDetails(1);
        moviePresenter.favorite(true);
    }

    @Test(expected = ExecutionException.class)
    public void deleteFavoriteTest() {
        Movie movie = getTestMovie();
        Response response = Response.success(movie);

        when(testAppComponent.getDataManager().getMovieDetails(1)).thenReturn(Observable.just(response));
        doThrow(ExecutionException.class).when(testAppComponent.getDataManager()).removeFromFavorites(movie);

        moviePresenter.getFilmDetails(1);
        moviePresenter.favorite(false);
    }

    private Movie getTestMovie() {
        Movie movie = new Movie();
        movie.setRate(2);
        movie.setOverview("smth");
        movie.setId(1);
        movie.setTitle("Test film");
        movie.setReleaseDate("2016");

        return movie;
    }
}
