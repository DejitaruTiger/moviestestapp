package com.riko.cinema;

import com.riko.cinema.dagger.TestAppComponent;
import com.riko.cinema.dagger.TestComponentRule;
import com.riko.cinema.data.api.model.MovieList;
import com.riko.cinema.ui.search.ISearchView$$State;
import com.riko.cinema.ui.search.SearchPresenter;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.annotations.NonNull;
import io.reactivex.annotations.Nullable;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.schedulers.ExecutorScheduler;
import io.reactivex.plugins.RxJavaPlugins;
import okhttp3.MediaType;
import okhttp3.ResponseBody;
import okio.BufferedSource;
import retrofit2.Response;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


public class SearchPresenterTest extends BaseTest {
    TestAppComponent testAppComponent = new TestAppComponent();
    @Rule public TestComponentRule testComponentRule = new TestComponentRule(testAppComponent);

    @Mock ISearchView$$State searchView$$State;
    SearchPresenter searchPresenter;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);

        searchPresenter = new SearchPresenter();
        searchPresenter.setViewState(searchView$$State);
    }

    @Test
    public void searchTest() {
        MovieList movieList = new MovieList();
        Response response = Response.success(movieList);
        Observable<Response<MovieList>> obs = Observable.just(response);

        when(testAppComponent.getDataManager().searchMovies(anyString())).thenReturn(obs);
        searchPresenter.search(searchRequest());

        verify(searchView$$State).showProgress();
        verify(searchView$$State).hideProgress();
        verify(searchView$$State).showSearchResults(movieList);
    }

    @Test
    public void searchTest_Error() {
        Observable<Response<MovieList>> obs = Observable.just(Response.error(404, new ResponseBody() {
            @Nullable
            @Override
            public MediaType contentType() {
                return null;
            }

            @Override
            public long contentLength() {
                return 0;
            }

            @Override
            public BufferedSource source() {
                return null;
            }
        }));

        when(testAppComponent.getDataManager().searchMovies(anyString())).thenReturn(obs);
        searchPresenter.search(searchRequest());

        verify(searchView$$State).showProgress();
        verify(searchView$$State).hideProgress();
        verify(searchView$$State).showToast("Error " + 404);
    }

    @Test
    public void showFavoriteMoviesTest() {
        searchPresenter.showFavoriteMovies();

        verify(testAppComponent.getDataManager()).getFavoriteMoviesList();
        verify(searchView$$State).showFavoriteMovies(testAppComponent.getDataManager().getFavoriteMoviesList());
    }

    @Test
    public void showMovieDetails() {
        searchPresenter.showMovieDetails(1);
        verify(searchView$$State).showMovieDetails(1);
    }

    private String searchRequest() {
        return "pirates";
    }
}
