package com.riko.cinema.dagger;

import android.content.Context;

import com.riko.cinema.SearchPresenterTest;
import com.riko.cinema.data.DataManager;
import com.riko.cinema.data.db.model.DbMovieDao;
import com.riko.cinema.di.components.AppComponent;
import com.riko.cinema.util.Utils;

import org.mockito.Mock;
import org.mockito.Mockito;

import javax.inject.Singleton;

import dagger.Component;



public class TestAppComponent implements AppComponent {
    private DataManager dataManager = Mockito.mock(DataManager.class);
    private Context context = Mockito.mock(Context.class);
    private DbMovieDao dbMovieDao = Mockito.mock(DbMovieDao.class);
    private Utils utils = Mockito.mock(Utils.class);

    @Override
    public Context getContext() {
        return context;
    }

    @Override
    public DataManager getDataManager() {
        return dataManager;
    }

    @Override
    public DbMovieDao getDbMovieDao() {
        return dbMovieDao;
    }

    @Override
    public Utils getUtils() {
        return utils;
    }
}
