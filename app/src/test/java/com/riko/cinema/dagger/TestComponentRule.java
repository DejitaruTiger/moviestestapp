package com.riko.cinema.dagger;

import android.support.annotation.NonNull;

import com.riko.cinema.App;
import com.riko.cinema.di.components.AppComponent;

import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;


public class TestComponentRule implements TestRule{
    private AppComponent appComponent;

    public TestComponentRule(@NonNull AppComponent component) {
        this.appComponent = component;
    }

    @Override
    public Statement apply(Statement base, Description description) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                App.setAppComponent(appComponent);
                base.evaluate();
            }
        };
    }
}
